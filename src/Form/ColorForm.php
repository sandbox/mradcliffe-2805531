<?php

namespace Drupal\typed_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\typed_widget\Form\TypedElementBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Color Example form.
 */
class ColorForm extends FormBase implements ContainerInjectionInterface {

  /**
   * Typed Element Builder.
   *
   * @var \Drupal\typed_widget\Form\TypedElementBuilder
   */
  protected $elementBuilder;

  /**
   * Typed Data Manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * ColorForm initialize method.
   *
   * @param \Drupal\typed_widget\Form\TypedElementBuilder $elementBuilder
   *   The typed widget element builder service.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typedDataManager
   *   The typed data manager.
   */
  public function __construct(TypedElementBuilder $elementBuilder, TypedDataManagerInterface $typedDataManager) {
    $this->elementBuilder = $elementBuilder;
    $this->typedDataManager = $typedDataManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'typed_example_color_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['example'] = $this->elementBuilder->getElementFor('typed_example_type');

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $definition = $this->typedDataManager->createDataDefinition('typed_example_type');
    $data = $this->typedDataManager->create($definition, $values['example']);
  }

  /**
   * {@inheritdoc}
   */
  static public function create(ContainerInterface $container) {
    return new static(
      $container->get('typed_widget.element_builder'),
      $container->get('typed_data_manager')
    );
  }

}
