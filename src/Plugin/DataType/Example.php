<?php

namespace Drupal\typed_example\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * @DataType(
 *   id = "typed_example_type",
 *   label = @Translation("Example"),
 *   definition_class = "\Drupal\typed_example\TypedData\ExampleDefinition"
 * )
 */
class Example extends Map {}
