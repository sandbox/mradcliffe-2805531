<?php

namespace Drupal\Tests\typed_example\Unit\Plugin\DataType;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\Plugin\DataType\FloatData;
use Drupal\Tests\UnitTestCase;
use Drupal\typed_example\Plugin\DataType\Color;
use Drupal\typed_example\TypedData\ColorDefinition;
use Prophecy\Argument;

/**
 * Test the Color data type class.
 *
 * @group typed_example
 */
class ColorTest extends UnitTestCase {

  /**
   * @var \Drupal\typed_example\Plugin\DataType\Color
   */
  protected $color;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {

    // The color definition class is fairly safe to use without mocking.
    $definition = ColorDefinition::create('typed_example_color');
    $definition->setClass('\Drupal\typed_example\Plugin\DataType\Color');

    // Instantiate a new color data type.
    $this->color = new Color($definition);

    $floatDefinition = DataDefinition::create('float')
      ->setClass('\Drupal\Core\TypedData\Plugin\DataType\FloatData');

    // Mock Typed Data Manager.
    $typedDataProphecy = $this->prophesize('\Drupal\Core\TypedData\TypedDataManagerInterface');
    $typedDataProphecy
      ->getPropertyInstance(Argument::any(), Argument::type('string'), Argument::any())
      ->will(function($args) use ($floatDefinition) {
        $data = new FloatData($floatDefinition);
        if ($args[1] === 'red') {
          $data->setValue(255);
        }
        else {
          $data->setValue(0.0);
        }
        return $data;
      });

    $container = new ContainerBuilder();
    $container->set('typed_data_manager', $typedDataProphecy->reveal());
    \Drupal::setContainer($container);

    $this->color->setValue([255, 0, 0]);
  }

  /**
   * Assert that getString returns the proper output.
   */
  public function testGetString() {
    $this->assertEquals('255 0 0', $this->color->getString());
  }

  /**
   * Assert that getCSSString returns the proper output.
   */
  public function testGetCssString() {
    $this->assertEquals('rgba(255,0,0,100)', $this->color->getCssString());
  }

  /**
   * Assert that exception is thrown when a non-numeric value is provided.
   *
   * @expectedException \InvalidArgumentException
   */
  public function testInvalidArgumentException() {
    $this->color->getCssString('invalid');
  }

}
