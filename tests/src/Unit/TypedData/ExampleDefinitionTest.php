<?php

namespace Drupal\Tests\typed_example\Unit\TypedData;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\typed_example\TypedData\ExampleDefinition;

/**
 * Test the Example definition.
 *
 * @group typed_example
 */
class ExampleDefinitionTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $typedDataProphecy = $this->prophesize('\Drupal\Core\TypedData\TypedDataManagerInterface');
    $container = new ContainerBuilder();
    $container->set('typed_data_manager', $typedDataProphecy->reveal());
    \Drupal::setContainer($container);
  }

  /**
   * Assert that properties exist.
   */
  public function testGetPropertyDefinitions() {
    $definition = new ExampleDefinition();
    $properties = $definition->getPropertyDefinitions();

    $this->assertEquals('Name', $properties['name']->getLabel());
    $this->assertEquals('Primary Color', $properties['primary']->getLabel());
    $this->assertEquals('Secondary Colors', $properties['secondary']->getLabel());
  }

}
