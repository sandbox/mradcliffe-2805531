<?php

namespace Drupal\typed_example\TypedData;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * An example of a complex data definition representing a color.
 */
class ColorDefinition extends ComplexDataDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = &$this->propertyDefinitions;

      $info['red'] = DataDefinition::create('float')
        ->setLabel('Red')
        ->setRequired(TRUE)
        ->addConstraint('Range', ['min' => 0, 'max' => 255]);
      $info['green'] = DataDefinition::create('float')
        ->setLabel('Green')
        ->setRequired(TRUE)
        ->addConstraint('Range', ['min' => 0, 'max' => 255]);
      $info['blue'] = DataDefinition::create('float')
        ->setLabel('Blue')
        ->setRequired(TRUE)
        ->addConstraint('Range', ['min' => 0, 'max' => 255]);
    }

    return $this->propertyDefinitions;
  }

}
