<?php

namespace Drupal\typed_example\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * @DataType(
 *   id = "typed_example_color",
 *   label = @Translation("Example Color"),
 *   definition_class = "\Drupal\typed_example\TypedData\ColorDefinition",
 *   list_class = "\Drupal\typed_example\Plugin\DataType\ExampleColorItemList"
 * )
 */
class Color extends Map {

  /**
   * Get a CSS RGB string of the value.
   *
   * @param int $opacity
   *   An optional opacity value. The default is 100.
   *
   * @return string
   *   A string in the format 'rgba(x,y,z,N)'.
   */
  public function getCssString($opacity = 100) {
    if (!is_numeric($opacity)) {
      throw new \InvalidArgumentException('Opacity is not a valid value.');
    }

    $value = 'rgba(' . $this->get('red')->getValue();
    $value .= ',' . $this->get('green')->getValue();
    $value .= ',' . $this->get('blue')->getValue();
    $value .= ',' . $opacity . ')';

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getString() {
    $value = $this->get('red')->getValue();
    $value .= ' ' . $this->get('green')->getValue();
    $value .= ' ' . $this->get('blue')->getValue();
    return $value;
  }

}
