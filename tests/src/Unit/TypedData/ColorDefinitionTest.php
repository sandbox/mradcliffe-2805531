<?php

namespace Drupal\Tests\typed_example\Unit\TypedData;

use Drupal\Tests\UnitTestCase;
use Drupal\typed_example\TypedData\ColorDefinition;

/**
 * Test that the property definitions exist for Color Definition.
 *
 * @group typed_example
 */
class ColorDefinitionTest extends UnitTestCase {

  /**
   * Assert that properties exist.
   */
  public function testGetPropertyDefinitions() {
    $definition = new ColorDefinition();
    $properties = $definition->getPropertyDefinitions();

    $this->assertEquals('Red', $properties['red']->getLabel());
    $this->assertEquals('Green', $properties['green']->getLabel());
    $this->assertEquals('Blue', $properties['blue']->getLabel());
  }

}
