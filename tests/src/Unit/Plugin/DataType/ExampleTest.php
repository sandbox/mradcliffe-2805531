<?php

namespace Drupal\Tests\typed_example\Unit\Plugin\DataType;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\Plugin\DataType\FloatData;
use Drupal\Core\TypedData\Plugin\DataType\StringData;
use Drupal\Tests\UnitTestCase;
use Drupal\typed_example\Plugin\DataType\Example;
use Drupal\typed_example\TypedData\ExampleDefinition;
use Prophecy\Argument;

/**
 * Test the Example data type.
 *
 * @group typed_example
 */
class ExampleTest extends UnitTestCase {

  protected $example;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {

    $definition = ExampleDefinition::create('typed_example_type');
    $definition->setClass('\Drupal\typed_example\Plugin\DataType\Example');

    // Instantiate a new Example type.
    $this->example = new Example($definition);

    $stringDefinition = DataDefinition::create('string');
    $stringDefinition->setClass('\Drupal\Core\TypedData\Plugin\DataType\StringData');
    $floatDefinition = DataDefinition::create('float');
    $floatDefinition->setClass('\Drupal\Core\TypedData\Plugin\DataType\FloatData');
    $floatData = new FloatData($floatDefinition);
    $floatData->setValue(100);
    $stringData = new StringData($stringDefinition);
    $stringData->setValue('Test example');

    // Mock the color data type.
    $colorProphecy = $this->prophesize('\Drupal\typed_example\Plugin\DataType\Color');
    $colorProphecy->get('red')->willReturn($floatData);
    $color = $colorProphecy->reveal();

    // Mock the color list item.
    $listProphecy = $this->prophesize('\Drupal\typed_example\Plugin\DataType\ExampleColorItemList');
    $listProphecy->get(0)->willReturn($color);
    $list = $listProphecy->reveal();

    // Mock Typed Data Manager.
    $typedDataProphecy = $this->prophesize('\Drupal\Core\TypedData\TypedDataManagerInterface');
    $typedDataProphecy->getPropertyInstance(Argument::any(), 'primary', Argument::any())
      ->willReturn($color);
    $typedDataProphecy->getPropertyInstance(Argument::any(), 'secondary', Argument::any())
      ->willReturn($list);
    $typedDataProphecy->getPropertyInstance(Argument::any(), 'red', Argument::any())
      ->willReturn($floatData);
    $typedDataProphecy->getPropertyInstance(Argument::any(), 'name', Argument::any())
      ->willReturn($stringData);

    $container = new ContainerBuilder();
    $container->set('typed_data_manager', $typedDataProphecy->reveal());
    \Drupal::setContainer($container);

    $this->example->setValue([
      'name' => 'Test example',
      'primary' => $color,
      'secondary' => $list,
    ]);
  }

  /**
   * Assert that the data type class is returned when getting property.
   *
   * @param string $class_name
   *   The class name to test.
   * @param string $property_name
   *
   * @dataProvider propertyTestProvider
   */
  public function testProperty($class_name, $property_name) {
    $this->assertInstanceOf($class_name, $this->example->get($property_name));
  }

  /**
   * Get the test parameters for testProperty.
   *
   * @return array
   *   An array of test parameters.
   */
  public function propertyTestProvider() {
    return [
      ['\Drupal\Core\TypedData\Plugin\DataType\StringData', 'name'],
      ['\Drupal\typed_example\Plugin\DataType\Color', 'primary'],
      ['\Drupal\Core\TypedData\Plugin\DataType\ItemList', 'secondary'],
    ];
  }

}
