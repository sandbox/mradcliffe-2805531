<?php

namespace Drupal\typed_example\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\ItemList;

/**
 * @DataType(
 *   id = "typed_example_list",
 *   label = @Translation("Example Color List"),
 *   definition_class = "\Drupal\Core\TypedData\ListDataDefinition"
 * )
 */
class ExampleColorItemList extends ItemList {}
