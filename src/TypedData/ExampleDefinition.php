<?php

namespace Drupal\typed_example\TypedData;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;

/**
 * An example of a complex data definition.
 */
class ExampleDefinition extends ComplexDataDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = &$this->propertyDefinitions;
      $info['name'] = DataDefinition::create('string')
        ->setLabel('Name')
        ->setDescription('A string property that is required.')
        ->setRequired(TRUE);
      $info['primary'] = ColorDefinition::create('typed_example_color')
        ->setLabel('Primary Color')
        ->setDescription('A complex data type as a child property.');
      $info['secondary'] = ListDataDefinition::create('typed_example_color')
        ->setLabel('Secondary Colors')
        ->setDescription('A list of complex data types as a child property.');
    }

    return $this->propertyDefinitions;
  }

}

